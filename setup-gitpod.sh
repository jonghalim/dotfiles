#!/usr/bin/bash
# zsh
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true
sudo chsh -s $(which zsh)

# git
sudo apt-get install fzf

git config --global alias.ci 'commit -v'
git config --global alias.alias '!git config --list | egrep "^alias.+"'
git config --global alias.st 'status'
git config --global alias.s 'status -s'
git config --global alias.cm 'commit'
git config --global alias.br 'branch'
git config --global alias.l log '--color --graph --pretty=format:"%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%C(bold blue)<%an>%Creset" --abbrev-commit'
git config --global alias.co '!git checkout $(git branch -vv | grep -v "^\\*" | fzf | awk "{print $1}")'
git config --global alias.a '!git add $(git status -s | fzf -m | awk "{print $2}")'
git config --global alias.df 'diff --cached'
git config --global alias.ua '!git reset HEAD $(git status -s | fzf -m | awk "{print $2}")'

# bashrc
echo "
alias gst='git status'
alias gco='git checkout'
alias gcob='git checkout -b'
" > ~/.bashrc
